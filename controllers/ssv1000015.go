//Version: v1.0.0.0
package controllers

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv1000015/constant"
	"git.forms.io/isaving/sv/ssv1000015/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000015Controller struct {
    controllers.CommTCCController
}

func (*Ssv1000015Controller) ControllerName() string {
	return "Ssv1000015Controller"
}

// @Desc ssv1000015 controller
// @Author
// @Date 2020-12-20
func (c *Ssv1000015Controller) Ssv1000015() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000015Controller.Ssv1000015 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000015I := &models.SSV1000015I{}
	if err :=  models.UnPackRequest(c.Req.Body, ssv1000015I); err != nil {
		c.SetServiceError(errors.New(err, constants.ERRCODE2))
		return
	}
  	if err := ssv1000015I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000015 := &services.Ssv1000015Impl{} 
    ssv1000015.New(c.CommTCCController)
	ssv1000015.Ssv1000015I = ssv1000015I
	ssv1000015Compensable := services.Ssv1000015Compensable


	proxy, err := aspect.NewDTSProxy(ssv1000015, ssv1000015Compensable, c.DTSCtx)
	if err != nil {
		log.Errorf("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv1000015I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD,"DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv1000015O, ok := rsp.(*models.SSV1000015O); ok {
		if responseBody, err := models.PackResponse(ssv1000015O); err != nil {
			c.SetServiceError(errors.New(err, constants.ERRCODE1))
		} else {
			c.SetAppBody(responseBody)
		}
	}
}
// @Title Ssv1000015 Controller
// @Description ssv1000015 controller
// @Param Ssv1000015 body models.SSV1000015I true body for SSV1000015 content
// @Success 200 {object} models.SSV1000015O
// @router /create [post]
func (c *Ssv1000015Controller) SWSsv1000015() {
	//Here is to generate API documentation, no need to implement methods
}
