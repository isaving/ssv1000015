////Version: v1.0.0.0
package routers

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/isaving/sv/ssv1000015/controllers"
	"git.forms.io/universe/common/event_handler/register"
	"github.com/astaxie/beego"
)

// @Desc Select the appropriate transaction route according to TOPIC
// @Author
// @Date 2020-12-20
func InitRouter() error {

	eventRouteReg := register.NewEventHandlerRegister()

	bc := beego.AppConfig

	eventRouteReg.Router(bc.String(constant.TopicPrefix + "ssv1000015"),
		&controllers.Ssv1000015Controller{}, "Ssv1000015")

	return nil
}

// @Desc transaction router for swagger
// @Author
// @Date 2020-12-20
func init() {

	ns := beego.NewNamespace("isaving/v1/ssv1000015",
		beego.NSNamespace("/ssv1000015",
			beego.NSInclude(
				&controllers.Ssv1000015Controller{},
			),
		),
	)
	beego.AddNamespace(ns)
}
