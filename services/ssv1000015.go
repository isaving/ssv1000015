//Version: v1.0.0.0
package services

import (
	"git.forms.io/isaving/models"
	constants "git.forms.io/isaving/sv/ssv1000015/constant"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
)
var Ssv1000015Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv1000015",
	ConfirmMethod: "ConfirmSsv1000015",
	CancelMethod:  "CancelSsv1000015",
}

type Ssv1000015 interface {
	TrySsv1000015(*models.SSV1000015I) (*models.SSV1000015O, error)
	ConfirmSsv1000015(*models.SSV1000015I) (*models.SSV1000015O, error)
	CancelSsv1000015(*models.SSV1000015I) (*models.SSV1000015O, error)
}

type Ssv1000015Impl struct {
    services.CommonTCCService

    Spr0000004O         *models.SPR0000004O
    Spr0000004I         *models.SPR0000004I

    Ssv1000015O         *models.SSV1000015O
    Ssv1000015I         *models.SSV1000015I
}
// @Desc Ssv1000015 process
// @Author
// @Date 2020-12-20
func (impl *Ssv1000015Impl) TrySsv1000015(ssv1000015I *models.SSV1000015I) (ssv1000015O *models.SSV1000015O, err error) {

	impl.Ssv1000015I = ssv1000015I

	if err := impl.PR000004(); nil != err {
		return nil, err
	}

	ssv1000015O = &models.SSV1000015O{
		State: "OK",
	}

	return ssv1000015O, nil
}

func (impl *Ssv1000015Impl) ConfirmSsv1000015(ssv1000015I *models.SSV1000015I) (ssv1000015O *models.SSV1000015O, err error) {
	log.Debug("Start confirm ssv1000015")
	return nil, nil
}

func (impl *Ssv1000015Impl) CancelSsv1000015(ssv1000015I *models.SSV1000015I) (ssv1000015O *models.SSV1000015O, err error) {
	log.Debug("Start cancel ssv1000015")
	return nil, nil
}

func (impl *Ssv1000015Impl) PR000004() error {
	request := impl.Ssv1000015I
	rspBody, err := request.PackRequest()
	if nil != err {
		return errors.New(err, constants.ERRCODE1)
	}
	_, err = impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_ID_COMMON, constants.PR000004, rspBody);
	if err != nil{
		log.Errorf("PR000004, err:%v",err)
		return err
	}

	return nil
}