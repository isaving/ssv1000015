package services

import (
	"errors"
	"fmt"
	"git.forms.io/isaving/models"
	jsoniter "github.com/json-iterator/go"
	. "reflect"
	"testing"
)

func (this *Ssv1000015Impl) RequestSyncServiceWithDCN(dstDcn, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

//这个要改成service的结构
func (this *Ssv1000015Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	return RequestService(serviceKey, requestData)
}

func (this *Ssv1000015Impl) RequestServiceWithDCN(dstDcn, serviceKey string, requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
	responseData = []byte(`{"Code":0,"Data":{"BS-LEGO":["C03101"]},"Message":"","errorCode":0,"errorMsg":"","response":{"BS-LEGO":["C03101"]}}`)
	return
}

func TestService(t *testing.T) {
	runTest(Ssv1000015Impl{}, "TrySsv1000015")
	runTest(Ssv1000015Impl{}, "ConfirmSsv1000015")
	runTest(Ssv1000015Impl{}, "CancelSsv1000015")
}

//报文头
var SrcAppProps = map[string]string{
	"TxUserId":   "1001",
	"TxDeptCode": "1001",
}

//输入请求
var request = []models.SSV1000015I{
	{
		Account:"1",
		AResult:[]models.SSV1000015IAResult{
			{
				InterestType :"1",
				StrategyId        :"1",
				ItemId            :"1",
				Rate              :1,
				FloatValue        :2,
				InterestMax       :2,
				InterestMin       :2,
				CalcType          :"1",
				ExecType          :"1",
				MonthDays         :"1",
				YearDays          :"1",
				DecimalFlag       :"1",
				AccruePeriodType  :"1",
				AccruePeriodValue :2,
				SettlePeriodType  :"1",
				SettlePeriodValue :2,
			},
		},
	},
	{
		Account:"1",
		AResult:[]models.SSV1000015IAResult{
			{
				InterestType :"1",
				StrategyId        :"1",
				ItemId            :"1",
				Rate              :1,
				FloatValue        :2,
				InterestMax       :2,
				InterestMin       :2,
				CalcType          :"1",
				ExecType          :"1",
				MonthDays         :"1",
				YearDays          :"1",
				DecimalFlag       :"1",
				AccruePeriodType  :"1",
				AccruePeriodValue :2,
				SettlePeriodType  :"1",
				SettlePeriodValue :2,
			},
		},
	},
}


//使用反引号可以直接换行 还不会被带双引号影响
var response = map[string]interface{}{
	//这个key 是你调用RequestSyncServiceElementKey()这个函数传的serviceKey的字符串 不一定是topic 好好看看自己的代码传的字符串是啥
	"PR000004":`{""errorCode":"0","errorMsg":"success","response:{"State":"Ok"}}`,

}

//func (this *Ssv1000015Impl)RequestServiceWithDCN(dstDcn, serviceKey string,requestData []byte, srcAppProps map[string]string) (responseData []byte, dstAppProps map[string]string, err error) {
//	if serviceKey == "DlsuDcnLists" {
//
//	}
//}

var errServiceKey []string

//初始化
func init() {
	for serviceKey := range response {
		errServiceKey = append(errServiceKey, serviceKey)
	}
}

var currentIndex = 0
var passed bool

//排列组合返回报文
func RequestService(serviceKey string, reqBytes []byte) (responseData []byte, err error) {

	if serviceKey == errServiceKey[currentIndex] && passed {
		currentIndex++
		if currentIndex >= len(errServiceKey) {
			currentIndex = 0
		}
		return nil, errors.New("error")
	}
	switch response[serviceKey].(type) {
	case string:
		responseData = []byte(response[serviceKey].(string))
	default:
		responseData = getByte(response[serviceKey])
	}
	return
}

func getByte(v interface{}) (bt []byte) {
	bt, _ = jsoniter.Marshal(struct {
		Form [1]struct{ FormData interface{} }
	}{Form: [1]struct{ FormData interface{} }{{v}}})
	return
}

func runTest(serviceStruct interface{}, method string) {
	param := make([]Value, 1)
	for _, req := range request {
		param[0] = ValueOf(&req)
		passed = false
		for i := 0; i < len(response); i++ {
			callMethod(serviceStruct, method, param)
			passed = true
		}
	}

}

func callMethod(serviceStruct interface{}, method string, params []Value) {
	service := New(TypeOf(serviceStruct))
	service.Elem().FieldByName("SrcAppProps").Set(ValueOf(SrcAppProps))
	ret := service.MethodByName(method).Call(params)
	fmt.Printf(" 返回 [ %v ] \n 错误 [ %v ]\n\n", ret[0], ret[1])
}


