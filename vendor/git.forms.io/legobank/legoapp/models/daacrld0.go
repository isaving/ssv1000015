package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRLD0I struct {
	HostTranSerialNo	string ` validate:"required,max=34"`
	HostTranSeq			string ` validate:"required,max=34"`
	HostTranInq			string ` validate:"required,max=6"`
	//SetNo				string
}

type DAACRLD0O struct {
	TxAcctnDt         string
	TxTm              string
	HostTranSerialNo  string
	HostTranSeq       string
	HostTranInq       string
	RcrdacctAcctnDt   string
	RcrdacctAcctnTm   string
	LiqdDt            string
	LiqdTm            string
	TxOrgNo           string
	AgenOrgNo         string
	TxTelrNo          string
	AuthTelrNo        string
	RchkTelrNo        string
	LunchChnlTypCd    string
	AccessChnlTypCd   string
	TerminalNo        string
	BizSysNo          string
	TranCd            string
	LiqdBizTypCd      string
	BizKindCd         string
	BizClsfCd         string
	DebitCrdtFlg      string
	OpenAcctOrgNo     string
	MbankAcctFlg      string
	CustNo            string
	SvngAcctNo        string
	CardNoOrAcctNo    string
	LoanAcctiAcctNo   string
	TermNo            string
	FinTxAmtTypCd     string
	CurCd             string
	TxnAmt            float64
	AcctBal           float64
	CashTxRcptpymtCd  string
	ValueDate         string
	FrnexcgStlManrCd  string
	PostvReblnTxFlgCd string
	RvrsTxFlgCd       string
	OrginlTxAcctnDt   string
	OrginlHostTxSn    string
	OrginlTxTelrNo    string
	CustMgrTelrNo     string
	MessageCode       string
	AbstractCode      string
	Abstract          string
	PmitRvrsFlg       string
	ChnlRvrsCtrlFlgCd string
	GvayLiqdFlg       string
	MndArapFlg        string
	LastMaintDate     string
	LastMaintTime     string
	LastMaintBrno     string
	LastMaintTell     string
	Status            string
	TccState          int
	StateAndRgnCd     string
	MbankFlg          string
	CashrmtFlgCd      string
	OthbnkBnkNo       string
	TxDtlTypCd        string
	MerchtNo          string
	AcctBookNo        string
	TermNumber        int
	LoanUsageCd       string
}

type DAACRLD0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRLD0I
}

type DAACRLD0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRLD0O
}

type DAACRLD0RequestForm struct {
	Form []DAACRLD0IDataForm
}

type DAACRLD0ResponseForm struct {
	Form []DAACRLD0ODataForm
}

// @Desc Build request message
func (o *DAACRLD0RequestForm) PackRequest(DAACRLD0I DAACRLD0I) (responseBody []byte, err error) {

	requestForm := DAACRLD0RequestForm{
		Form: []DAACRLD0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLD0I",
				},
				FormData: DAACRLD0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRLD0RequestForm) UnPackRequest(request []byte) (DAACRLD0I, error) {
	DAACRLD0I := DAACRLD0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLD0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLD0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRLD0ResponseForm) PackResponse(DAACRLD0O DAACRLD0O) (responseBody []byte, err error) {
	responseForm := DAACRLD0ResponseForm{
		Form: []DAACRLD0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLD0O",
				},
				FormData: DAACRLD0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRLD0ResponseForm) UnPackResponse(request []byte) (DAACRLD0O, error) {

	DAACRLD0O := DAACRLD0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLD0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLD0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRLD0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
