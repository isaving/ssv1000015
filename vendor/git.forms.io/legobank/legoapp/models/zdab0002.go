package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type ZDAB0002I struct {
	Total			int		//审批的笔数
	LoanDubilNo		string //贷款借据号
	MakelnAplySn    string //放款申请流水号
	CustNo          string //客户编号
	LoanStatus      string //贷款状态
	ApproveUserId   string //审批人员工号
	ApproveView     string //审批意见
	ApproveComment  string //审批意见说明

}

type ZDAB0002O struct {
	LoanDubilNo string `json:"LoanDubilNo"` // 贷款借据号
	State string
}

// @Desc Build request message
func (o *ZDAB0002I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *ZDAB0002I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *ZDAB0002O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *ZDAB0002O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *ZDAB0002I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

