package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRBC0I struct {
	AcctgAcctNo		string	`validate:"required,max=20"`
	LoanStatus		string	`validate:"required,max=2"`
	PeriodNum		int64	`validate:"required"`
	BalanceType		string	`validate:"required"`
}

type DAACRBC0O struct {
	AcctCreateDate string      `json:"AcctCreateDate"`
	AcctType       string      `json:"AcctType"`
	AcctgAcctNo    string      `json:"AcctgAcctNo"`
	AcctingOrgID   string      `json:"AcctingOrgId"`
	Balance        int         `json:"Balance"`
	BalanceType    string      `json:"BalanceType"`
	CavAmt         int         `json:"CavAmt"`
	CurrIntStDate  string      `json:"CurrIntStDate"`
	Currency       string      `json:"Currency"`
	LastMaintBrno  string      `json:"LastMaintBrno"`
	LastMaintDate  string      `json:"LastMaintDate"`
	LastMaintTell  string      `json:"LastMaintTell"`
	LastMaintTime  string      `json:"LastMaintTime"`
	LastTranDate   string      `json:"LastTranDate"`
	LoanStatus     string      `json:"LoanStatus"`
	MgmtOrgID      string      `json:"MgmtOrgId"`
	OrgCreate      string      `json:"OrgCreate"`
	PeriodNum      int         `json:"PeriodNum"`
	TccState       interface{} `json:"TccState"`
}

type DAACRBC0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRBC0I
}

type DAACRBC0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRBC0O
}

type DAACRBC0RequestForm struct {
	Form []DAACRBC0IDataForm
}

type DAACRBC0ResponseForm struct {
	Form []DAACRBC0ODataForm
}

// @Desc Build request message
func (o *DAACRBC0RequestForm) PackRequest(DAACRBC0I DAACRBC0I) (responseBody []byte, err error) {

	requestForm := DAACRBC0RequestForm{
		Form: []DAACRBC0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRBC0I",
				},
				FormData: DAACRBC0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRBC0RequestForm) UnPackRequest(request []byte) (DAACRBC0I, error) {
	DAACRBC0I := DAACRBC0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRBC0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRBC0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRBC0ResponseForm) PackResponse(DAACRBC0O DAACRBC0O) (responseBody []byte, err error) {
	responseForm := DAACRBC0ResponseForm{
		Form: []DAACRBC0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRBC0O",
				},
				FormData: DAACRBC0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRBC0ResponseForm) UnPackResponse(request []byte) (DAACRBC0O, error) {

	DAACRBC0O := DAACRBC0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRBC0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRBC0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRBC0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
