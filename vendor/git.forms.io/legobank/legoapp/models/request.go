package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
)

type FormHead struct {
	FormId string
}

type Body struct {
	Form []Form
}
type Form struct {
	FormHead FormHead
	FormData map[string]interface{}
}

func (o *Body) PackRequest(mapOne map[string]interface{}) (responseBody []byte, err error) {

	requestForm := Body{
		Form: []Form{
			{
				FormHead: FormHead{
					FormId: "",
				},
				FormData: mapOne,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

func (o *Body) UnPackRequest(request []byte) (map[string]interface{}, error) {
	mapOne := make(map[string]interface{})
	if err := json.Unmarshal(request, o); nil != err {
		return mapOne, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return mapOne, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

type BodyArr struct {
	Form []FormArr
}

type FormArr struct {
	FormHead FormHead
	FormData []map[string]interface{}
}

func (o *BodyArr) PackRequest(mapArr []map[string]interface{}) (responseBody []byte, err error) {

	requestForm := BodyArr{
		Form: []FormArr{
			{
				FormHead: FormHead{
					FormId: "",
				},
				FormData: mapArr,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

func (o *BodyArr) UnPackRequest(request []byte) ([]map[string]interface{}, error) {
	mapArr := make([]map[string]interface{},0)
	if err := json.Unmarshal(request, o); nil != err {
		return mapArr, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return mapArr, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

type BodyMult struct {
	Form []FormMult
}

type FormMult struct {
	FormHead FormHead
	FormData interface{}
}

type ParamSub struct {
	ParamGroup string `json:"ParmGroup"`
}

type ParamResp struct {
	Form []struct {
		FormHead struct {
			FormId string
		}
		FormData map[string]string
	}
}
type ErrorResponseForm struct {
	Form []ErrorDataForm
}

type ErrorDataForm struct {
	FormHead CommonFormHead	`json:"FormHead"`
	FormData ErrorData		`json:"FormData"`
}

type ErrorData struct {
	RetMsgCode 	string
	RetMessage	string
}


func (o *ErrorResponseForm) PackRequest(ErrorData ErrorData) (responseBody []byte, err error) {

	requestForm := ErrorResponseForm{
		Form: []ErrorDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "",
				},
				FormData: ErrorData,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

func (o *ErrorResponseForm) UnPackRequest(request []byte) (ErrorData, error) {
	ErrorData := ErrorData{}
	if err := json.Unmarshal(request, o); nil != err {
		return ErrorData, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return ErrorData, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}
