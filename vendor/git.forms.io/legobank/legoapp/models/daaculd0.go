package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACULD0I struct {
	TxAcctnDt         string
	TxTm              string
	HostTranSerialNo  string
	HostTranSeq       string
	HostTranInq       string
	RcrdacctAcctnDt   string
	RcrdacctAcctnTm   string
	LiqdDt            string
	LiqdTm            string
	TxOrgNo           string
	AgenOrgNo         string
	TxTelrNo          string
	AuthTelrNo        string
	RchkTelrNo        string
	LunchChnlTypCd    string
	AccessChnlTypCd   string
	TerminalNo        string
	BizSysNo          string
	TranCd            string
	LiqdBizTypCd      string
	BizKindCd         string
	BizClsfCd         string
	DebitCrdtFlg      string
	OpenAcctOrgNo     string
	MbankAcctFlg      string
	CustNo            string
	SvngAcctNo        string
	CardNoOrAcctNo    string
	LoanAcctiAcctNo   string
	TermNo            string
	FinTxAmtTypCd     string
	CurCd             string
	TxnAmt            float64
	AcctBal           float64
	CashTxRcptpymtCd  string
	ValueDate         string
	FrnexcgStlManrCd  string
	PostvReblnTxFlgCd string
	RvrsTxFlgCd       string
	OrginlTxAcctnDt   string
	OrginlHostTxSn    string
	OrginlTxTelrNo    string
	CustMgrTelrNo     string
	MessageCode       string
	AbstractCode      string
	Abstract          string
	PmitRvrsFlg       string
	ChnlRvrsCtrlFlgCd string
	GvayLiqdFlg       string
	MndArapFlg        string
	LastMaintDate     string
	LastMaintTime     string
	LastMaintBrno     string
	LastMaintTell     string
	Status            string
	TccState          int
	StateAndRgnCd     string
	MbankFlg          string
	CashrmtFlgCd      string
	OthbnkBnkNo       string
	TxDtlTypCd        string
	MerchtNo          string
	AcctBookNo        string
	TermNumber        int
	LoanUsageCd       string
}

type DAACULD0O struct {

}

type DAACULD0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACULD0I
}

type DAACULD0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACULD0O
}

type DAACULD0RequestForm struct {
	Form []DAACULD0IDataForm
}

type DAACULD0ResponseForm struct {
	Form []DAACULD0ODataForm
}

// @Desc Build request message
func (o *DAACULD0RequestForm) PackRequest(DAACULD0I DAACULD0I) (responseBody []byte, err error) {

	requestForm := DAACULD0RequestForm{
		Form: []DAACULD0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULD0I",
				},
				FormData: DAACULD0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACULD0RequestForm) UnPackRequest(request []byte) (DAACULD0I, error) {
	DAACULD0I := DAACULD0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACULD0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULD0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACULD0ResponseForm) PackResponse(DAACULD0O DAACULD0O) (responseBody []byte, err error) {
	responseForm := DAACULD0ResponseForm{
		Form: []DAACULD0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACULD0O",
				},
				FormData: DAACULD0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACULD0ResponseForm) UnPackResponse(request []byte) (DAACULD0O, error) {

	DAACULD0O := DAACULD0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACULD0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACULD0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACULD0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
