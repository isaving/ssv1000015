package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACRLB0I struct {
	AcctgAcctNo		string ` validate:"required,max=20"`
	BalanceType		float64
}

type DAACRLB0O struct {
	AcctgAcctNo      string      `json:"AcctgAcctNo"`
	Balance          interface{} `json:"Balance"`
	BalanceType      int         `json:"BalanceType"`
	BalanceYesterday interface{} `json:"BalanceYesterday"`
	LastMaintBrno    interface{} `json:"LastMaintBrno"`
	LastMaintDate    interface{} `json:"LastMaintDate"`
	LastMaintTell    interface{} `json:"LastMaintTell"`
	LastMaintTime    interface{} `json:"LastMaintTime"`
	TccState         int         `json:"TccState"`
}

type DAACRLB0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACRLB0I
}

type DAACRLB0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACRLB0O
}

type DAACRLB0RequestForm struct {
	Form []DAACRLB0IDataForm
}

type DAACRLB0ResponseForm struct {
	Form []DAACRLB0ODataForm
}

// @Desc Build request message
func (o *DAACRLB0RequestForm) PackRequest(DAACRLB0I DAACRLB0I) (responseBody []byte, err error) {

	requestForm := DAACRLB0RequestForm{
		Form: []DAACRLB0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLB0I",
				},
				FormData: DAACRLB0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACRLB0RequestForm) UnPackRequest(request []byte) (DAACRLB0I, error) {
	DAACRLB0I := DAACRLB0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLB0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLB0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACRLB0ResponseForm) PackResponse(DAACRLB0O DAACRLB0O) (responseBody []byte, err error) {
	responseForm := DAACRLB0ResponseForm{
		Form: []DAACRLB0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACRLB0O",
				},
				FormData: DAACRLB0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACRLB0ResponseForm) UnPackResponse(request []byte) (DAACRLB0O, error) {

	DAACRLB0O := DAACRLB0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACRLB0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACRLB0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACRLB0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
