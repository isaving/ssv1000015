package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC060009I struct {
	AcctgAcctNo		string `validate:"required,max=20"`
	BankNo			string `valid:"Required;MaxSize(30)"` //机构号
	IntRateNo		string `valid:"Required;MaxSize(10)"` //利率编号
	ExpFlag			string `valid:"Required;MaxSize(1)"`  //失效标识
}

type AC060009O struct {
	IntRateNo     string
	BankNo        string
	Currency      string
	IntRate       float64
	EffectDate    string
	ExpFlag       string
	DayModfyFlag  string
	IntRateName   string
	Flag1         string
	Flag2         string
	Back1         float64
	Back2         float64
	Back3         string
	Back4         string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	IntRateMarket string
	IntRateUnit   string
}

type AC060009IDataForm struct {
	FormHead CommonFormHead
	FormData AC060009I
}

type AC060009ODataForm struct {
	FormHead CommonFormHead
	FormData AC060009O
}

type AC060009RequestForm struct {
	Form []AC060009IDataForm
}

type AC060009ResponseForm struct {
	Form []AC060009ODataForm
}

// @Desc Build request message
func (o *AC060009RequestForm) PackRequest(AC060009I AC060009I) (responseBody []byte, err error) {

	requestForm := AC060009RequestForm{
		Form: []AC060009IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060009I",
				},
				FormData: AC060009I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC060009RequestForm) UnPackRequest(request []byte) (AC060009I, error) {
	AC060009I := AC060009I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC060009I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060009I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC060009ResponseForm) PackResponse(AC060009O AC060009O) (responseBody []byte, err error) {
	responseForm := AC060009ResponseForm{
		Form: []AC060009ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC060009O",
				},
				FormData: AC060009O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC060009ResponseForm) UnPackResponse(request []byte) (AC060009O, error) {

	AC060009O := AC060009O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC060009O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC060009O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC060009I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
