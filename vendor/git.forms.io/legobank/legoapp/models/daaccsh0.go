package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCSH0I struct {
	AcctgNo            string `validate:"required,max=20"`
	AcctgAcctNo        string `validate:"required,max=20"`
	ProdType           string
	AcctType           string
	Lvls               float64
	Sigma              float64
	IntRate            string
	IntPlanNo          string
	AcctBal            string
	CumulativeProdAmt  interface{}
	StartDate          string
	AsOfDate           string
	RestCycleStartDate string
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string
}

type DAACCSH0O struct {
	AcctgNo            string `validate:"required,max=20"`
	AcctgAcctNo        string `validate:"required,max=20"`
	ProdType           string
	AcctType           string
	Lvls               float64
	Sigma              float64
	IntRate            string
	IntPlanNo          string
	AcctBal            string
	CumulativeProdAmt  float64
	StartDate          string
	AsOfDate           string
	RestCycleStartDate string
	LastMaintDate      string
	LastMaintTime      string
	LastMaintBrno      string
	LastMaintTell      string
}

type DAACCSH0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCSH0I
}

type DAACCSH0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCSH0O
}

type DAACCSH0RequestForm struct {
	Form []DAACCSH0IDataForm
}

type DAACCSH0ResponseForm struct {
	Form []DAACCSH0ODataForm
}

// @Desc Build request message
func (o *DAACCSH0RequestForm) PackRequest(DAACCSH0I DAACCSH0I) (responseBody []byte, err error) {

	requestForm := DAACCSH0RequestForm{
		Form: []DAACCSH0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCSH0I",
				},
				FormData: DAACCSH0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCSH0RequestForm) UnPackRequest(request []byte) (DAACCSH0I, error) {
	DAACCSH0I := DAACCSH0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCSH0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCSH0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCSH0ResponseForm) PackResponse(DAACCSH0O DAACCSH0O) (responseBody []byte, err error) {
	responseForm := DAACCSH0ResponseForm{
		Form: []DAACCSH0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCSH0O",
				},
				FormData: DAACCSH0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCSH0ResponseForm) UnPackResponse(request []byte) (DAACCSH0O, error) {

	DAACCSH0O := DAACCSH0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCSH0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCSH0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCSH0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
