package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type IL8R0008IDataForm struct {
	FormHead CommonFormHead
	FormData map[string]interface{}
}

type IL8R0008ODataForm struct {
	FormHead CommonFormHead
	FormData IL8R0008O
}

type IL8R0008RequestForm struct {
	Form []IL8R0008IDataForm
}

type IL8R0008ResponseForm struct {
	Form []IL8R0008ODataForm
}

type IL8R0008I struct {
	LoanDubilNo                       string  `json:"LoanDubilNo"`
	Pridnum                           int     `json:"Pridnum"`
	CustNo                            string  `json:"CustNo"`
	KeprcdStusCd                      string  `json:"KeprcdStusCd"`
	OperSorcCd                        string  `json:"OperSorcCd"`
	PrinOvdueBgnDt                    string  `json:"PrinOvdueBgnDt"`
	IntrOvdueBgnDt                    string  `json:"IntrOvdueBgnDt"`
	CurrPeriodUseGraceDays            int     `json:"CurrPeriodUseGraceDays"`
	ActlstTotlAmt                     float64  `json:"ActlstTotlAmt"`
	ActlRepayPrin                     float64  `json:"ActlRepayPrin"`
	ActlRepayIntr                     float64  `json:"ActlRepayIntr"`
	FinlRepayDt                       string  `json:"FinlRepayDt"`
	ActlPyfDt                         string  `json:"ActlPyfDt"`
	RepayDt                           string  `json:"RepayDt"`
	FinlModfyDt                       string  `json:"FinlModfyDt"`
	FinlModfyTm                       string  `json:"FinlModfyTm"`
	FinlModfyOrgNo                    string  `json:"FinlModfyOrgNo"`
	FinlModfyTelrNo                   string  `json:"FinlModfyTelrNo"`
	PageNo                            int     `json:"PageNo"`
	PageRecCount                      int     `json:"PageRecCount"`
}

type IL8R0008O struct {
	Records                      []IL8R0008ORecords
	PageNo                       int  `json:"PageNo"`
	PageRecCount                 int  `json:"PageRecCount"`
}

type IL8R0008ORecords struct {
	LoanDubilNo                       string  `json:"LoanDubilNo"`
	Pridnum                           int     `json:"Pridnum"`
	CustNo                            string  `json:"CustNo"`
	KeprcdStusCd                      string  `json:"KeprcdStusCd"`
	OperSorcCd                        string  `json:"OperSorcCd"`
	PrinOvdueBgnDt                    string  `json:"PrinOvdueBgnDt"`
	IntrOvdueBgnDt                    string  `json:"IntrOvdueBgnDt"`
	CurrPeriodUseGraceDays            int     `json:"CurrPeriodUseGraceDays"`
	ActlstTotlAmt                     float64  `json:"ActlstTotlAmt"`
	ActlRepayPrin                     float64  `json:"ActlRepayPrin"`
	ActlRepayIntr                     float64  `json:"ActlRepayIntr"`
	FinlRepayDt                       string  `json:"FinlRepayDt"`
	ActlPyfDt                         string  `json:"ActlPyfDt"`
	RepayDt                           string  `json:"RepayDt"`
	FinlModfyDt                       string  `json:"FinlModfyDt"`
	FinlModfyTm                       string  `json:"FinlModfyTm"`
	FinlModfyOrgNo                    string  `json:"FinlModfyOrgNo"`
	FinlModfyTelrNo                   string  `json:"FinlModfyTelrNo"`
}

// @Desc Build request message
func (o *IL8R0008RequestForm) PackRequest(il8r0008I map[string]interface{}) (responseBody []byte, err error) {

	requestForm := IL8R0008RequestForm{
		Form: []IL8R0008IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL8R0008I",
				},

				FormData: il8r0008I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *IL8R0008RequestForm) UnPackRequest(request []byte) (map[string]interface{}, error) {
	il8r0008I := make(map[string]interface{})
	if err := json.Unmarshal(request, o); nil != err {
		return il8r0008I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il8r0008I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *IL8R0008ResponseForm) PackResponse(il8r0008O IL8R0008O) (responseBody []byte, err error) {
	responseForm := IL8R0008ResponseForm{
		Form: []IL8R0008ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "IL8R0008O",
				},
				FormData: il8r0008O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *IL8R0008ResponseForm) UnPackResponse(request []byte) (IL8R0008O, error) {

	il8r0008O := IL8R0008O{}

	if err := json.Unmarshal(request, o); nil != err {
		return il8r0008O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return il8r0008O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *IL8R0008I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
