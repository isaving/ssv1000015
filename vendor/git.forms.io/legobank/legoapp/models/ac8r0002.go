package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type AC8R0002I struct {
	AcctgAcctNo string
	PeriodNum   int
	Flag		interface{}
}

type AC8R0002O struct {
	PageTotCount	int
	PageNo			int
	Records			[]AC8R0002ORecords
}

type AC8R0002ORecords struct {
	RecordNo            int64
	AcctgAcctNo         string
	BalanceType         string
	PeriodNum           int64
	CurrIntStDate       string
	CurrIntEndDate      string
	UnpaidInt           float64
	RepaidInt           float64
	AccmWdAmt           float64
	AccmIntSetlAmt      float64
	UnpaidIntAmt        float64
	RepaidIntAmt        float64
	AlrdyTranOffshetInt float64
	DcValueInt          float64
	CavInt              float64
	OnshetInt           float64
	FrzAmt              float64
	Status              string
	LastCalcDate        string
	LastMaintDate       string
	LastMaintTime       string
	LastMaintBrno       string
	LastMaintTell       string
	AcruUnstlIntr       float64
	AccmCmpdAmt         float64

}

type AC8R0002IDataForm struct {
	FormHead CommonFormHead
	FormData AC8R0002I
}

type AC8R0002ODataForm struct {
	FormHead CommonFormHead
	FormData AC8R0002O
}

type AC8R0002RequestForm struct {
	Form []AC8R0002IDataForm
}

type AC8R0002ResponseForm struct {
	Form []AC8R0002ODataForm
}

// @Desc Build request message
func (o *AC8R0002RequestForm) PackRequest(AC8R0002I AC8R0002I) (responseBody []byte, err error) {

	requestForm := AC8R0002RequestForm{
		Form: []AC8R0002IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC8R0002I",
				},
				FormData: AC8R0002I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *AC8R0002RequestForm) UnPackRequest(request []byte) (AC8R0002I, error) {
	AC8R0002I := AC8R0002I{}
	if err := json.Unmarshal(request, o); nil != err {
		return AC8R0002I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC8R0002I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *AC8R0002ResponseForm) PackResponse(AC8R0002O AC8R0002O) (responseBody []byte, err error) {
	responseForm := AC8R0002ResponseForm{
		Form: []AC8R0002ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "AC8R0002O",
				},
				FormData: AC8R0002O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *AC8R0002ResponseForm) UnPackResponse(request []byte) (AC8R0002O, error) {

	AC8R0002O := AC8R0002O{}

	if err := json.Unmarshal(request, o); nil != err {
		return AC8R0002O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return AC8R0002O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *AC8R0002I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
