package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type DAACCTR0I struct {
	IntRateNo     string `validate:"required,max=20"`
	BankNo        string
	Currency      string
	IntRate       float64
	EffectDate    string
	ExpFlag       string
	DayModfyFlag  string
	IntRateName   string
	Flag1         string
	Flag2         string
	Back1         float64
	Back2         float64
	Back3         string
	Back4         string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	IntRateMarket string
	IntRateUnit   string
}

type DAACCTR0O struct {
	IntRateNo     string
	BankNo        string
	Currency      string
	IntRate       float64
	EffectDate    string
	ExpFlag       string
	DayModfyFlag  string
	IntRateName   string
	Flag1         string
	Flag2         string
	Back1         float64
	Back2         float64
	Back3         string
	Back4         string
	LastMaintDate string
	LastMaintTime string
	LastMaintBrno string
	LastMaintTell string
	IntRateMarket string
	IntRateUnit   string
}

type DAACCTR0IDataForm struct {
	FormHead CommonFormHead
	FormData DAACCTR0I
}

type DAACCTR0ODataForm struct {
	FormHead CommonFormHead
	FormData DAACCTR0O
}

type DAACCTR0RequestForm struct {
	Form []DAACCTR0IDataForm
}

type DAACCTR0ResponseForm struct {
	Form []DAACCTR0ODataForm
}

// @Desc Build request message
func (o *DAACCTR0RequestForm) PackRequest(DAACCTR0I DAACCTR0I) (responseBody []byte, err error) {

	requestForm := DAACCTR0RequestForm{
		Form: []DAACCTR0IDataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCTR0I",
				},
				FormData: DAACCTR0I,
			},
		},
	}

	responseBody, err = json.Marshal(requestForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing request message
func (o *DAACCTR0RequestForm) UnPackRequest(request []byte) (DAACCTR0I, error) {
	DAACCTR0I := DAACCTR0I{}
	if err := json.Unmarshal(request, o); nil != err {
		return DAACCTR0I, errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCTR0I, errors.New("UnPackRequest failed.", constant.REQUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

// @Desc Build response message
func (o *DAACCTR0ResponseForm) PackResponse(DAACCTR0O DAACCTR0O) (responseBody []byte, err error) {
	responseForm := DAACCTR0ResponseForm{
		Form: []DAACCTR0ODataForm{
			{
				FormHead: CommonFormHead{
					FormId: "DAACCTR0O",
				},
				FormData: DAACCTR0O,
			},
		},
	}

	responseBody, err = json.Marshal(responseForm)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *DAACCTR0ResponseForm) UnPackResponse(request []byte) (DAACCTR0O, error) {

	DAACCTR0O := DAACCTR0O{}

	if err := json.Unmarshal(request, o); nil != err {
		return DAACCTR0O, errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	if len(o.Form) < 1 {
		return DAACCTR0O, errors.New("UnPackResponse failed.", constant.RSPUNPACKERR)
	}

	return o.Form[0].FormData, nil
}

func (w *DAACCTR0I) Validate() error {
	validate := validator.New()
	return validate.Struct(w)
}
