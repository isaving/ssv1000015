//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package constant

const (
	EVENT_HANDLER_VERSION = "v0.2.1"
)

const (
	MESSAGE_TYPE_JSON = "json"
)

const (
	CONTROLLER_NAME_GET_METHOD = "ControllerName"
)
