//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package event

//transaction header
type TxnEventHeader struct {
	Service string `json:"service"`
	Code    string `json:"code"`
	Message string `json:"message"`
}
