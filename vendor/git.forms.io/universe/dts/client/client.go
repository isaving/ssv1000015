//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package client

import (
	constant "git.forms.io/universe/dts/common/const"
	"reflect"
	"sync"
)

// @Desc keep service's TCC's method name and Timeout time
type Compensable struct {
	TryMethod     string
	ConfirmMethod string
	CancelMethod  string
	Timeout       int64
}

// @Desc the Invocation Information of transaction
type TransactionInvocation struct {
	Sync                      sync.RWMutex
	TxnInstance               interface{}
	ReflectValueOfTxnInstance reflect.Value
	StoreParams               []reflect.Value
	RootXid                   string
	BranchXid                 string
	State                     constant.TxnStat
	CancelSuspend             bool
	CompensableTxn            Compensable
	NumOfParam                int
}

// @Desc get the state of transaction
// @Return state
func (t *TransactionInvocation) GetState() constant.TxnStat {
	t.Sync.RLock()
	defer t.Sync.RUnlock()

	return t.State
}

// @Desc set the state of transaction
// @Param state
func (t *TransactionInvocation) SetState(state constant.TxnStat) {
	t.Sync.Lock()
	defer t.Sync.Unlock()

	t.State = state
}

func (t *TransactionInvocation) IsState(state constant.TxnStat) bool {
	t.Sync.Lock()
	defer t.Sync.Unlock()

	return t.State == state
}

// @Desc set the CancelSuspend of transaction to true
func (t *TransactionInvocation) SetCancelSuspend() {
	t.Sync.Lock()
	defer t.Sync.Unlock()
	t.CancelSuspend = true
}

// @Desc get the CancelSuspend of transaction
// @Return CancelSuspend
func (t *TransactionInvocation) IsCancelSuspend() bool {
	t.Sync.RLock()
	defer t.Sync.RUnlock()
	return t.CancelSuspend
}
