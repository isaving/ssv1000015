//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package controllers

import (
	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/common/event_handler/base"
	constant "git.forms.io/universe/dts/common/const"
	"git.forms.io/universe/solapp-sdk/compensable"
	jsoniter "github.com/json-iterator/go"
	"strconv"
)

// @Desc a DTSClientBaseHandler includes BaseHandler, ErrorCode and DTSCtx
type DTSClientBaseHandler struct {
	base.BaseHandler
	ErrorCode int
	DTSCtx    *compensable.TxnCtx
}

// @Desc the Prepare() of DTSClientBaseHandler, which finds out DTS information from Request
func (b *DTSClientBaseHandler) EventHandlePrepare() {
	var rootXid string
	var parentXid string
	var dtsAgentAddress string
	var traceId string
	var spanId string
	var parentSpanId string

	if nil != b.Req && nil != b.Req.AppProps {
		rootXid = b.Req.AppProps[constant.TXN_PROPAGATE_ROOT_XID_KEY]
		parentXid = b.Req.AppProps[constant.TXN_PROPAGATE_PARENT_XID_KEY]
		dtsAgentAddress = b.Req.AppProps[constant.TXN_PROPAGATE_DTS_AGENT_ADDRESS]

		traceId = b.Req.AppProps[constant.TRACE_ID_KEY]
		spanId = b.Req.AppProps[constant.SPAN_ID_KEY]
		parentSpanId = b.Req.AppProps[constant.PARENT_SPAN_ID_KEY]
	}

	spanCtx := &compensable.SpanContext{
		TraceId:      traceId,
		SpanId:       spanId,
		ParentSpanId: parentSpanId,
	}

	txnCtx := compensable.CmpTxnCtsInstance.NewTxnCtx()

	txnCtx.SpanCtx = spanCtx

	//if "" != rootXid && "" != parentXid && "" != dtsAgentAddress {
	txnCtx.RootXid = rootXid
	txnCtx.ParentXid = parentXid
	txnCtx.DtsAgentAddress = dtsAgentAddress
	b.DTSCtx = txnCtx
	//}
}

// @Desc the Finish() of DTSClientBaseHandler, which describes response according to transaction's result
func (b *DTSClientBaseHandler) EventHandleFinish() {
	if nil == b.Rsp && b.Err != nil {
		json := jsoniter.ConfigCompatibleWithStandardLibrary
		res, _ := json.Marshal(base.BuildErrorResponse(b.Err.Error()))

		var appProps map[string]string
		if nil != b.Req && nil != b.Req.AppProps {
			appProps = b.Req.AppProps
		} else {
			appProps = make(map[string]string)
		}

		appProps["RetMsgCode"] = strconv.Itoa(constant.CommonError)
		appProps["RetMessage"] = b.Err.Error()
		b.Rsp = &client.UserMessage{
			AppProps: appProps,
			Body:     res,
		}
	}

	// clean DTSCtx
	b.DTSCtx = nil
}
