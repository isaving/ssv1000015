//Version: v1.0.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000024I struct {
	KeyVersion		string	`validate:"required",json:"KeyVersion"`		//钥版本号
	BeVerifiedPsw	string	`validate:"required",json:"BeVerifiedPsw"`	//	旧支付密码
	NewPsw			string	`validate:"required",json:"NewPsw"`			//	新支付密码
	AccPsw			string	`validate:"required",json:"AccPsw"`			//db passworld
	AgreementType	string	`validate:"required",json:"AgreementType"`	//	合约类型
	AgreementId		string	`validate:"required",json:"AgreementId"`	//	合约号
}

type SSV1000024O struct {
	State	string
}

// @Desc Build request message
func (o *SSV1000024I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000024I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000024O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000024O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000024I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000024I) GetServiceKey() string {
	return "sv100024"
}
