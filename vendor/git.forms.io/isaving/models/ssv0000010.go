//Version: v1.0.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV0000010I struct {
	DccMdsNm	string `json:"DccMdsNm"`//介质号码
	DccMdsTyp	string `json:"DccMdsTyp"`//介质类型
	DccAgrmt	string `validate:"required",json:"DccAgrmt"`	//合约号
	DccAgrmtTyp	string `validate:"required",json:"DccAgrmtTyp"`	//合约类型
	DccMode		string `validate:"required",json:"DccMode"`		//控制方向设置
	DccMechName	string `validate:"required",json:"DccMechName"`//控制机构名称
	DccDocNm	string `json:"DccDocNm"`//控制文书号

}

type SSV0000010O struct {
	Status		string
}

// @Desc Build request message
func (o *SSV0000010I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV0000010I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV0000010O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV0000010O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV0000010I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV0000010I) GetServiceKey() string {
	return "sv000011"
}
