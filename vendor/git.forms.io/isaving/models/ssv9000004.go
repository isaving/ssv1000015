//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV9000004I struct {
	QryTpy        string `valid:"Required;MaxSize(30)"`//查询类型
	MediaType     string `valid:"MaxSize(4)"`//介质类型
	MediaNm       string `valid:"MaxSize(40)"`//介质号码
	AgreementId   string `valid:"MaxSize(30)"`//合约号
	AgreementType string `valid:"MaxSize(5)"`//合约类型
}

type SSV9000004O struct {
	RecordTotNum int
	Records []SSV9000004ORecord
}

type SSV9000004ORecord struct {
	AgreementID     string //合约号
	AgreementType   string //合约类型
	Currency        string //币种
	CashtranFlag    string //钞汇标志
	AgreementStatus string //合约状态	 0-正常;1-销户计息;2-结清;3-解约
	AccOpnDt        string //开户日期
	CstmrCntctPh    string //手机号
	CstmrCntctAdd   string //联系地址
	CstmrCntctEm    string //邮箱
	AccPsw          string //账户密码
	WdrwlMthd       string //支取方式	0-密码;1-印鉴;2-签字、3-指纹;4-密码+印鉴;5-密码+签字;6-密码+指纹;7-印鉴+签字;8-印鉴+指纹;9-签字+指纹
	AccAcount       string //核算账号
	CstmrId         string //客户编号
	FreezeType      string //冻结状态	0-正常;1-合约冻结/账户冻结;2-金额冻结
	TmpryPrhibition string //暂禁状态	0-正常，1-暂禁
	DepcreFlag      string //借贷记控制标志	N-正常;D-只借记;C-只贷记;F-不允许借贷记
	AccuntNme       string //账户姓名
}

// @Desc Build request message
func (o *SSV9000004I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV9000004I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV9000004O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV9000004O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV9000004I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV9000004I) GetServiceKey() string {
	return "ssv9000004"
}
