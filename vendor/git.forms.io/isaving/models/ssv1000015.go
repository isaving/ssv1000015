//Version: v1.0.0.0
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SSV1000015I struct {
	Account string `json:"Account" validate:"required,max=32"`
	DcnId	string
	AResult []SSV1000015IAResult
}
type SSV1000015IAResult struct {
	InterestType      string
	StrategyId        string
	ItemId            string
	Rate              float64
	FloatValue        float64
	InterestMax       float64 `json:"InterestMax" validate:"required"`
	InterestMin       float64 `json:"InterestMin" validate:"required"`
	CalcType          string  `json:"CalcType" validate:"required"`
	ExecType          string  `json:"ExecType" validate:"required"`
	MonthDays         string  `json:"MonthDays" validate:"requiredx"`
	YearDays          string  `json:"YearDays" validate:"required"`
	DecimalFlag       string  `json:"DecimalFlag" validate:"required"`
	AccruePeriodType  string  `json:"AccruePeriodType" validate:"required"`
	AccruePeriodValue int     `json:"AccruePeriodValue" validate:"required"`
	SettlePeriodType  string  `json:"SettlePeriodType" validate:"required"`
	SettlePeriodValue int     `json:"SettlePeriodValue" validate:"required"`
}

type SSV1000015O struct {
	State string
}

// @Desc Build request message
func (o *SSV1000015I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SSV1000015I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SSV1000015O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SSV1000015O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SSV1000015I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SSV1000015I) GetServiceKey() string {
	return "sv100015"
}
